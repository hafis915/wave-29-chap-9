import { useEffect, useState } from "react";
import { Outlet, Link, useNavigate } from "react-router-dom";
import { Button } from 'reactstrap'
import './navbar.css'

export default function NavbarHome() {
    const navigate = useNavigate()
    const [isLogin, setIsLogin] = useState(null)

    // const isLogin = localStorage.getItem('token')
    useEffect(() => {
        let token = localStorage.getItem('token')
        setIsLogin(token)
    }, [])

    // useEffect(() => {
    //     console.log('=== rerender navbar ===')
    // }, [isLogin])
    function handleLoginButton() {
        if(isLogin) {
            setIsLogin(null)
            localStorage.removeItem('token')
        }else{
            navigate('/login')
        }
    }
    return (
        <>
            <nav className="navbar-home">
                Binar

                <div 
                className="navbar-home-center"
                style={{
                    display : 'flex',
                    gap : '15px',
                    alignItems : 'center',
                    width: '60%',
                    justifyContent : 'center'
                }}>
                    <Link to='/home'><p>Home</p></Link>
                    <Link to={'/student'} ><p >Students</p></Link>
                    {/* <a href="/home">Home 2</a> */}
                    <Link><p >Contact</p></Link>
                    
                </div>

                <div>
                    {
                        isLogin ? 
                        <Button onClick={handleLoginButton} className="btn-danger">Logout</Button>
                        :
                        <Button onClick={handleLoginButton} className="btn-danger">Login</Button>
                    }
                    
                </div>
            </nav>
            <Outlet />
        </>
    )
}