import {Navigate} from 'react-router-dom'


export default function  PrivateRoute({Children , ...rest} ) {
    const token = localStorage.getItem('token')

    if(!token) {
        return <Navigate to={'/login'} />
    }else {
        return <Children {...rest} />
    }
}