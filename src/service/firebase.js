// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCsQQAeQyjdAxaSEpRLQlxGvZCgbuKifqA",
  authDomain: "fsw-binar-wave-29.firebaseapp.com",
  projectId: "fsw-binar-wave-29",
  storageBucket: "fsw-binar-wave-29.appspot.com",
  messagingSenderId: "681933292538",
  appId: "1:681933292538:web:c6432d768826e551669a43",
  databaseURL : 'https://fsw-binar-wave-29-default-rtdb.asia-southeast1.firebasedatabase.app/'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app