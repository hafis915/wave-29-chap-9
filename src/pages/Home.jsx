import { Outlet } from 'react-router-dom'
import React, { useEffect, useState } from "react"

export default function Home() {
    const [isLogin , setIsLogin] = useState(false)

    useEffect(() => {
        let token = localStorage.getItem('token')
        if(token) {
            setIsLogin(true)
        }
    }, [])

    useEffect(() => {})

    return(
        <>
            <h1>Home</h1>
            { isLogin ? 
                <h1>LOGIN</h1>
                :
                <h1>BELUM LOGIN</h1>
            }
            <Outlet />
        </>
    )
}