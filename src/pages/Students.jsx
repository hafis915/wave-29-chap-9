import { Outlet } from 'react-router-dom'
import { useState, useEffect } from 'react'
import axios from 'axios'
export default function Student() {
    const [students, setStudents] = useState([])
    const [test, setTest] = useState('')

    useEffect(() => {
        // didMount
        // didUpdate
        // didUnmount

        fetchStudent()
        .then(res => {
            console.log(res, "<<ini res")
            setStudents(res.data)
        })
        .catch(err => {
            console.log(err)
        })

        // return () => { console.log( '=== unmounted ==')}
    }, [])


    async function fetchStudent() {
        try {
            let url = `http://localhost:4000/user`
            let data = await axios.get(url)
            return data.data
        } catch (error) {
            throw error
        }
    }

    return (
        <div>
            <h1>Student</h1>
            <table>
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Join Date
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        students && students.map((el,index) => {
                            return(
                                <tr key={index}>
                                    <td>{el.name}</td>
                                    <td>{el.joinDate}</td>
                                <td>EDIT | DELETE</td>
                            </tr>
                            )
                        })
                    }

                </tbody>

        </table>
        </div>
    )
}