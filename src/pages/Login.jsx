import {Input, Button} from 'reactstrap'
import { useState } from 'react'
import { getAuth, signInWithEmailAndPassword, GoogleAuthProvider, signInWithPopup } from 'firebase/auth'
import app from '../service/firebase'
import { useNavigate } from 'react-router'
const auth = getAuth(app)
const provider = new GoogleAuthProvider();

export default function Login() {
    const navigate = useNavigate()
    const [ credential , setCredential ] = useState({
        email : '',
        password : ''
    })
    const [error, setError] = useState('')
    async function handleLogin() {
        try {
            const login = await signInWithEmailAndPassword(auth, credential.email, credential.password)
            const token = login.user.accessToken
            localStorage.setItem('token', token)
            navigate('/home')
        } catch (error) {
            setError("Wrong Password / Email")
        }
    }

    async function loginWithGoogle() {
        auth.languageCode = 'it'
        signInWithPopup(auth, provider)
        .then(result => {
            const credential = GoogleAuthProvider.credentialFromResult(result);
            const token = credential.accessToken;
            // The signed-in user info.
            const user = result.user;
            console.log(user)
            console.log(token)
            localStorage.setItem('token', token)
            navigate('/home')

        })
        .catch(err => {
            console.log(err)
            setError('something wrong')
        })

    }

    function handleChangeInput(e, type) {

        let value = e.target.value
        let temp = {...credential}
        temp[type] = value
        setCredential(temp)
    }




    return (
        <div>
          <Input type="text" placeholder='email' value={credential.email} onChange={(e) => handleChangeInput(e, 'email')}/>
            <Input  type='password' placeholder='password' value={credential.password} onChange={(e) => handleChangeInput(e, 'password')} />
            <Button 
            className='btn-success' 
            onClick={handleLogin} 
            >Login</Button>

            <Button 
            onClick={loginWithGoogle}
            >Login With Google</Button>
            <p>{error}</p>
        </div>
    )
}