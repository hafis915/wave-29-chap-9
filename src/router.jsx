import { createBrowserRouter } from 'react-router-dom'
import Home from './pages/Home'
import Student from './pages/Students'
import NavbarHome from './components/Navbar'
import Login from './pages/Login'
import PrivateRoute from './PrivateRoute'
import Signup from './pages/Signup'

const router = createBrowserRouter(
    [
        {
            path : '/',
            element: <NavbarHome />,
            children :[
                {
                    path : '/student',
                    element : <PrivateRoute Children={Student} />
                },
                {
                    path : '/home',
                    element : <Home />
                },
                {
                    path: '/login',
                    element : <Login />
                },
                {
                    path : '/signup',
                    element : <Signup />
                }
            ]
        }
    ]
)


export default router